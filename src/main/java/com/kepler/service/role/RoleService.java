package com.kepler.service.role;

import com.kepler.pojo.Role;

import java.util.List;

/**
 * @author lenovo
 * 定义了与角色表服务相关的接口
 */
public interface RoleService {
    public List<Role> getRoleList();
}

