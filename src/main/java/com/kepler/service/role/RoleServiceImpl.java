package com.kepler.service.role;

import com.kepler.dao.BaseDao;
import com.kepler.dao.role.RoleDao;
import com.kepler.dao.role.RoleDaoImpl;
import com.kepler.pojo.Role;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author lenovo
 * 实现了与身份表服务相关接口的实现类
 */
public class RoleServiceImpl implements RoleService{

    private RoleDao roleDao;

    /**
     * 构造函数，初始化 roleDao, 使用 Dao 层所提供的方法
     */
    public RoleServiceImpl(){
        roleDao = new RoleDaoImpl();
    }

    /**
     * 获取身份信息列表
     * @return 身份信息列表
     */
    public List<Role> getRoleList() {
        Connection connection = null;
        List<Role> roleList = new ArrayList<Role>();

        try {
            connection = BaseDao.getConnection();
            roleList = roleDao.getRoleList(connection);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            BaseDao.closeResources(connection, null, null);
        }

        return roleList;
    }

    @Test
    public void test(){
        RoleService roleService = new RoleServiceImpl();
        List<Role> roleList = roleService.getRoleList();
        for (Role role : roleList) {
            System.out.println(role.getRoleName());
        }
    }
}
