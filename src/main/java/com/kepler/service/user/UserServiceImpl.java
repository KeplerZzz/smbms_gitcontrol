package com.kepler.service.user;

import com.kepler.dao.BaseDao;
import com.kepler.dao.user.UserDao;
import com.kepler.dao.user.UserDaoImpl;
import com.kepler.pojo.User;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lenovo
 * 实现了与用户表服务相关接口的实现类
 */
public class UserServiceImpl implements UserService{
    // 业务层都会调用dao层，所以我们要引入Dao层
    private UserDao userDao;

    public UserServiceImpl(){
        userDao = new UserDaoImpl();
    }

    public User login(String userCode, String password) {
        Connection connection = null;
        User user = null;

        try {
            connection = BaseDao.getConnection();
            user = userDao.getLoginUser(connection, userCode);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            BaseDao.closeResources(connection,null,null);
        }
        return user;
    }

    public boolean updatePassword(int id, String password) {
        Connection connection = null;
        boolean flag = false;

        // 修改密码
        try {
            connection = BaseDao.getConnection();
            if (userDao.updatePassword(connection, id, password) > 0){
                flag = true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            BaseDao.closeResources(connection, null, null);
        }

        return flag;
    }

    public int getUserCount(String userName, int userRole) {
        Connection connection = null;
        int count = 0;

        try {
            connection = BaseDao.getConnection();
            count = userDao.getUserCount(connection, userName, userRole);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            BaseDao.closeResources(connection,null,null);
        }
        return count;
    }

    public List<User> getUserList(String userName, int userRole, int currentPageNo, int pageSize) {
        Connection connection = null;
        List<User> userList = null;

        try {
            connection = BaseDao.getConnection();
            userList = new ArrayList<User>();
            userList = userDao.getUserList(connection, userName, userRole, currentPageNo, pageSize);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            BaseDao.closeResources(connection, null, null);
        }

        return userList;
    }

    public boolean addUser(User user) {
        Connection connection = null;
        int updateRows = 0;
        boolean flag = false;

        try {
            connection = BaseDao.getConnection();
            connection.setAutoCommit(false);
            updateRows = userDao.addUser(connection, user);
            connection.commit();
            connection.setAutoCommit(true);
            if (updateRows > 0){
                flag = true;
                System.out.println("insert success");
            } else {
                flag = false;
                System.out.println("insert failed");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            try {
                connection.rollback();
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            BaseDao.closeResources(connection,null,null);
        }

        return flag;
    }

    public boolean selectUserCodeExist(String userCode) {
        Connection connection = null;
        int updateRows = 0;
        boolean flag = false;

        try {
            connection = BaseDao.getConnection();
            updateRows = userDao.selectUserCodeExist(connection, userCode);
            if (updateRows == 1){
                flag = true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            BaseDao.closeResources(connection, null,null);
        }

        return flag;
    }

    public int deleteUser(int id) {
        Connection connection = null;
        int updateRows = 0;

        try {
            connection = BaseDao.getConnection();
            connection.setAutoCommit(false);
            updateRows = userDao.deleteUser(connection, id);
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException throwables) {
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            throwables.printStackTrace();
        } finally {
            BaseDao.closeResources(connection,null,null);
        }
        return updateRows;
    }

    public boolean modify(User user) {
        Connection connection = null;
        boolean flag = false;
        int updateRows = 0;

        try {
            connection = BaseDao.getConnection();
            connection.setAutoCommit(false);
            updateRows = userDao.modify(connection, user);
            connection.commit();
            connection.setAutoCommit(true);
            if (updateRows == 1) {
                flag = true;
            }
        } catch (SQLException throwables) {
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            throwables.printStackTrace();
        } finally {
            BaseDao.closeResources(connection, null,null);
        }

        return flag;
    }

    public User getUserById(int id) {
        Connection connection = null;
        User user = new User();

        try {
            connection = BaseDao.getConnection();
            user = userDao.getUserById(connection, id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            BaseDao.closeResources(connection, null,null);
        }

        return user;
    }
}
