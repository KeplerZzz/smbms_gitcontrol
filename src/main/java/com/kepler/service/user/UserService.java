package com.kepler.service.user;

import com.kepler.pojo.User;

import java.sql.SQLException;
import java.util.List;

/**
 * @author lenovo
 * 定义了一个与用户表服务相关的接口
 */
public interface UserService {
    /**
     * 通过用户名查询用户的信息（还有的对比密码没有实现）后期完善
     * @param userCode 用户名
     * @param password 密码
     * @return 该用户所有信息
     */
    public User login (String userCode, String password);

    /**
     * 修改用户密码服务
     * @param id 需要修改密码的用户编号
     * @param password 修改密码的用户密码
     * @return 数据库数据受影响的行数
     */
    public boolean updatePassword(int id, String password);

    /**
     * 查询用户的记录数量（可根据用户的用户名，和用户所扮演的身份进行查询）
     * @param userName 指定要查询的用户名
     * @param userRole 指定要查询的用户身份
     * @return 指定查询的数据库中的用户记录数量
     */
    public int getUserCount(String userName, int userRole);

    /**
     * 获取用户的列表信息
     * @param userName 指定要查询的用户名
     * @param userRole 指定要查询的用户身份
     * @param currentPageNo 当前的页数
     * @param pageSize 一个页面内记录信息的大小
     * @return 用户信息列表
     */
    public List<User> getUserList(String userName, int userRole, int currentPageNo, int pageSize);

    /**
     * 增加用户
     * @param user 所增加用户的信息
     * @return 是否插入成功
     */
    public boolean addUser(User user);

    /**
     * 通过用户编号查看数据库中是否存在重复的 userCode
     * @param userCode 用户编号
     * @return 是否存在相同的用户编号
     */
    public boolean selectUserCodeExist(String userCode);

    /**
     * 删除用户
     * @param id 用户 id
     * @return 受影响的行数
     * @throws SQLException 数据库异常
     */
    public int deleteUser(int id) throws SQLException;

    /**
     * 修改用户
     * @param user 用户信息对象实例
     * @return 是否修改成功
     * @throws SQLException 数据库异常
     */
    public boolean modify(User user) throws SQLException;

    /**
     * 通过 id 查找用户的信息
     * @param id 用户 id
     * @return User的实例对象
     * @throws SQLException 数据库异常
     */
    public User getUserById(int id) throws SQLException;
}
