package com.kepler.util;

/**
 * 工具类，保存一些信息（常量）
 * @author lenovo
 */
public class Constant {
    public final static String USER_SESSION = "userSession";
    public final static String MESSAGE = "message";
    public final static String METHOD = "method";
    public final static String SAVE_PASSWORD = "savepwd";
    public final static String DEAL_OLD_PASSWORD = "pwdmodify";
    public final static String USER_MANGER = "query";
    public final static String INSERT_USER = "add";
    public final static String GET_ROLE_LIST = "getrolelist";
    public final static String USER_CODE_EXIST = "ucexist";
    public final static String DELETE_USER = "deluser";
    public final static String USER_CODE = "userCode";
    public final static String DELETE_RESULT = "delResult";
    public final static String MODIFY_USER = "modify";
    public final static String MODIFY_EXE = "modifyexe";
    public final static String VIEW_PAGE = "view";
}
