package com.kepler.filter;

import com.kepler.pojo.User;
import com.kepler.util.Constant;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author lenovo
 */
public class SysFilter implements Filter {
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        // 过滤器，从 Session 中获取用户
        User user = (User) request.getSession().getAttribute(Constant.USER_SESSION);
        if (user == null){
            // 用户已经被移除，注销
            response.sendRedirect("/smbms_and_gitcontrol_war/error/error.jsp");
        } else {
            filterChain.doFilter(request,response);
        }
    }

    public void destroy() {

    }
}
