package com.kepler.dao.role;

import com.kepler.dao.BaseDao;
import com.kepler.pojo.Role;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lenovo
 * 身份表接口的实现类
 */
public class RoleDaoImpl implements RoleDao{

    /**
     * 获取角色列表
     * @param connection 数据库连接对象实例
     * @return 数据库中的用户的角色列表
     * @throws SQLException 数据库异常
     */
    public List<Role> getRoleList(Connection connection) throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Role> roleList = null;

        if (connection!=null) {
            String sql = "select * from smbms_role";
            Object[] params = {};
            roleList = new ArrayList<Role>();
            resultSet = BaseDao.execute(connection, preparedStatement, resultSet, sql, params);
            while (resultSet.next()){
                Role _role = new Role();
                _role.setRoleName(resultSet.getString("roleName"));
                _role.setId(resultSet.getInt("id"));
                _role.setRoleCode(resultSet.getString("roleCode"));
                roleList.add(_role);
            }
            BaseDao.closeResources(null, preparedStatement, resultSet);
        }
        return roleList;
    }
}
