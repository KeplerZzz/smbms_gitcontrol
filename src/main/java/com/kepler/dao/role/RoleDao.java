package com.kepler.dao.role;

import com.kepler.pojo.Role;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author lenovo
 * 定义了与身份表相关的接口
 */
public interface RoleDao {
    public List<Role> getRoleList(Connection connection) throws SQLException;
}
