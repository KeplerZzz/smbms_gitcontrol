package com.kepler.dao.user;

import com.kepler.pojo.Role;
import com.kepler.pojo.User;
import com.kepler.util.Constant;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author lenovo
 */
public interface UserDao {
    /**
     * 获取登录用户的信息
     * @param connection 数据库连接对象实例
     * @param userCode 登录用户名
     * @return 用户实例
     * @throws SQLException 数据库异常
     */
    public User getLoginUser(Connection connection, String userCode) throws SQLException;

    /**
     * 修改当前用户密码
     * @param connection 数据库连接对象实例
     * @param id 修改密码用户的编号
     * @param password 修改密码用户的密码
     * @return 数据库数据修改后受影响的行数
     * @throws SQLException 数据库异常
     */
    public int updatePassword(Connection connection, int id, String password) throws SQLException;

    /**
     * 获取用户信息的记录数量（可通过指定的用户名和用户身份进行查询）
     * @param connection 数据库连接对象实例
     * @param userName 指定需要进行查询的用户名
     * @param userRole 指定需要进行查询的用户身份
     * @return 通过指定进行查询的用户记录数量
     * @throws SQLException 数据库异常
     */
    public int getUserCount(Connection connection, String userName, int userRole) throws SQLException;

    /**
     * 获取用户的信息列表
     * @param connection 数据库连接对象实例
     * @param userName 指定需要进行查询的用户名
     * @param userRole 指定需要进行查询的用户身份
     * @param currentPageNo 当前的页数
     * @param pageSize 一页所能容纳的用户信息条数
     * @return 用户信息的列表
     * @throws SQLException 数据库异常
     */
    public List<User> getUserList(Connection connection, String userName, int userRole, int currentPageNo, int pageSize) throws SQLException;

    /**
     * 插入用户
     * @param connection 数据库连接对象实例
     * @param user 需要插入的用户信息对象实例
     * @return 受影响的行数
     * @throws SQLException 数据库异常
     */
    public int addUser(Connection connection, User user) throws SQLException;

    /**
     * 查询使用该用户编号的人员数
     * @param connection 数据库连接对象实例
     * @param userCode 用户的编号
     * @return 根据用户编号查询出的人员个数
     * @throws SQLException 数据库异常
     */
    public int selectUserCodeExist(Connection connection, String userCode) throws SQLException;

    /**
     * 删除用户
     * @param connection 数据库连接对象实例
     * @param id 用户的 id
     * @return 受影响的行数
     * @throws SQLException 数据库异常
     */
    public int deleteUser(Connection connection, int id) throws SQLException;

    /**
     * 修改用户信息
     * @param connection 数据库连接对象实例
     * @param user 需要修改的用户信息对象实例
     * @return 受影响的行数
     * @throws SQLException 数据库异常
     */
    public int modify(Connection connection, User user) throws SQLException;

    /**
     * 通过用户 id 获取用户信息
     * @param connection 数据库连接对象实例
     * @param id 用户 id
     * @return 用户信息的对象实例
     * @throws SQLException 数据库异常
     */
    public User getUserById(Connection connection, int id) throws SQLException;
}
