package com.kepler.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Arrays;
import java.util.Properties;

/**
 * @author lenovo
 * 操作数据库的公共类
 */
public class BaseDao {
    private static String driver;
    private static String url;
    private static String username;
    private static String password;

    // 静态代码块，类加载的时候就初始化了
    static{
        // 加载 properties 文件
        Properties properties = new Properties();
        // 通过类加载器读取对应的资源
        InputStream is = BaseDao.class.getClassLoader().getResourceAsStream("db.properties");

        try {
            properties.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }

        driver = properties.getProperty("driver");
        url = properties.getProperty("url");
        username = properties.getProperty("username");
        password = properties.getProperty("password");
    }

    /**
     * 获取连接数据库的对象实例
     * @return 数据库的对象实例 Connection
     */
    public static Connection getConnection(){
        Connection connection = null;
        try {
            // 添加数据库驱动
            Class.forName(driver);
            connection = DriverManager.getConnection(url, username, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    /**
     * 执行 Sql 的查询语句
     * @param connection 数据库的连接对象指代 DriverManager
     * @param sql 需要执行的 sql 语句，带？号的，因为该 sql 语句为预处理的 sql 语句
     * @param params 预处理 SQL 中的一些参数，指代预处理 sql 中的问号（ ？）
     * @param resultSet ResultSet 的实例
     * @param preparedStatement 执行语句的对象实例
     * @return 从数据库中查找出的数据的一个结果，为 ResultSet
     */
    public static ResultSet execute(Connection connection, PreparedStatement preparedStatement, ResultSet resultSet, String sql, Object[] params) throws SQLException {
        preparedStatement = connection.prepareStatement(sql);

        for (int i = 1; i <= params.length; i++) {
            preparedStatement.setObject(i, params[i-1]);
        }
        System.out.println("BaseDao->execute" + preparedStatement);
        resultSet = preparedStatement.executeQuery();
        return resultSet;
    }

    /**
     * 执行 Sql 中的增，删，改操作
     * @param connection 数据库的连接对象指代 DriverManager
     * @param sql 需要执行的 sql 语句，带？号的，因为该 sql 语句为预处理的 sql 语句
     * @param params 预处理 SQL 中的一些参数，指代预处理 sql 中的问号（ ？）
     * @param preparedStatement 执行语句的对象实例
     * @return 数据库中受影响数据的条数 (int)
     */
    public static int execute(Connection connection, PreparedStatement preparedStatement, String sql, Object[] params) throws SQLException {
        preparedStatement = connection.prepareStatement(sql);

        for (int i = 1; i <= params.length; i++) {
            preparedStatement.setObject(i, params[i-1]);
        }

        int updateRows = preparedStatement.executeUpdate();
        return updateRows;
    }

    /**
     * 释放资源
     * @param connection 数据库的连接对象指代 DriverManager
     * @param preparedStatement 执行语句的对象实例
     * @param resultSet ResultSet 的实例
     * @return 资源是否释放成功
     */
    public static boolean closeResources(Connection connection, PreparedStatement preparedStatement, ResultSet resultSet){
        boolean flag = true;
        if (resultSet != null){
            try {
                resultSet.close();
                // GC回收
                resultSet = null;
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                flag = false;
            }
        }
        if (preparedStatement != null){
            try {
                preparedStatement.close();
                // GC回收
                preparedStatement = null;
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                flag = false;
            }
        }
        if (connection != null){
            try {
                connection.close();
                // GC回收
                connection = null;
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                flag = false;
            }
        }
        return flag;
    }
}
